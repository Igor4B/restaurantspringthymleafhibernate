package com.restaurant.igi.Restaurantsht.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.igi.Restaurantsht.Entity.Cart;
import com.restaurant.igi.Restaurantsht.Entity.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Integer>{
	List<CartItem> findByCart( Cart cart);

}
