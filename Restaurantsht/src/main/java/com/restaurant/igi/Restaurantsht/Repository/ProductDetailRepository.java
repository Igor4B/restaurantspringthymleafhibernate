package com.restaurant.igi.Restaurantsht.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.igi.Restaurantsht.Entity.Product;


public interface ProductDetailRepository extends JpaRepository<Product, Integer>{
	
	Optional<Product> findById(Integer id);

}
