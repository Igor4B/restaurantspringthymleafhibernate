package com.restaurant.igi.Restaurantsht;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaurantshtApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantshtApplication.class, args);
	}

}
