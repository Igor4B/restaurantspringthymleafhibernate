package com.restaurant.igi.Restaurantsht.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.igi.Restaurantsht.Entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
