package com.restaurant.igi.Restaurantsht.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.restaurant.igi.Restaurantsht.Service.ProductService;
import com.restaurant.igi.Restaurantsht.Utils.CartContainer;


@Controller
public class MainController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired CartContainer cartContainer;
	
	 @RequestMapping("/")
	    public String indexPage(Model model) {
	        model.addAttribute("products", productService.getAllProducts());
	        return "index";
	    }
	 
	 @RequestMapping("cart/add/{id}")
	 public String addProductToCart(@PathVariable (value="id") int id) {
		cartContainer.addItem(productService.getProduct(id)); 
		System.out.println(cartContainer); 
		return "redirect:/";
		 
	 }
	 
	 @RequestMapping("/cart")
		public String cartView() {
			return "cart";
		}

	 @RequestMapping("/cart/clear")
	 public String cartClear()
	 {
		 cartContainer.clear();
		 return "redirect:/cart";
	 }
	 
	 @RequestMapping("/cart/remove/{id}")
	 public String removeProductFromCart(@PathVariable(value = "id") int id) {
		cartContainer.removeItem(id);  
		return "redirect:/cart";
		 
	 }

}
