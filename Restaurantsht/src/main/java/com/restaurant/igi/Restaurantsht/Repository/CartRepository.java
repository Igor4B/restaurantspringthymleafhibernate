package com.restaurant.igi.Restaurantsht.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.igi.Restaurantsht.Entity.Cart;
import com.restaurant.igi.Restaurantsht.Entity.User;

public interface CartRepository extends JpaRepository<Cart, Integer> {
		List<Cart> findByUser(User user);
}
