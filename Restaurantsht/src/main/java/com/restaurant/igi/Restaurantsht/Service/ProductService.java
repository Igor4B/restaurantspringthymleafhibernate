package com.restaurant.igi.Restaurantsht.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restaurant.igi.Restaurantsht.Entity.Product;
import com.restaurant.igi.Restaurantsht.Repository.ProductRepository;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }

    public Product getProduct(Integer id){
        return productRepository.getOne(id);
    }
}
