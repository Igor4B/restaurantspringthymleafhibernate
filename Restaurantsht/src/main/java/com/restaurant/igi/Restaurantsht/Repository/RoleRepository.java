package com.restaurant.igi.Restaurantsht.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.igi.Restaurantsht.Entity.Role;

public interface RoleRepository extends JpaRepository<Role, String> {

}
