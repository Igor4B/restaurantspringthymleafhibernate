package com.restaurant.igi.Restaurantsht.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.restaurant.igi.Restaurantsht.Entity.Product;

public interface ProductSearchRepository extends JpaRepository<Product, Integer> {
	//@Query(value = "select * from Products p where p.Name like %:keyword%",nativeQuery=true)
	//List<Product> findByKeyword(@Param("keyword") String keyword);

}
