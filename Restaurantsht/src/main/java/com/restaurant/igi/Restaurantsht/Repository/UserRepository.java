package com.restaurant.igi.Restaurantsht.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.restaurant.igi.Restaurantsht.Entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);

}
